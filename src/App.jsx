import { useState } from 'react'
import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import TrelloBoards from './components/trelloboards'
import Lists from './components/list'

import Header from './components/header'
function App() {
  return (
    <>
    
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path='/' element={<TrelloBoards />}/>
        <Route path='/boards/:boardId' element={<Lists />}/>
      </Routes>
    </BrowserRouter>

    </>
  )
}

export default App
