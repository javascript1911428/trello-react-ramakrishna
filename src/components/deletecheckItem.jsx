import React from "react";
import axios from "axios";
import { Button } from "@mui/material";

function DeleteCheckItem({ checklistId, checkItemId, onDelete }) {
  const handleDeleteCheckItem = () => {
    const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
    const token =
      "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

    axios
      .delete(
        `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${apiKey}&token=${token}`
      )
      .then((response) => {
        onDelete(checkItemId);
      })
      .catch((error) => {
        console.error("Error deleting check item:", error);
      });
  };

  return (
    <Button
      variant="outlined"
      onClick={handleDeleteCheckItem}
    >
      DELETE
    </Button>
  );
}

export default DeleteCheckItem;
