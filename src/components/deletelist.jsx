import React from "react";
import { Button } from "@mui/material";
import axios from "axios";

function DeleteList({ list, onDelete }) {
  const handleDelete = () => {
    const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
    const token =
      "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

    axios
      .put(
        `https://api.trello.com/1/lists/${list.id}/closed?key=${apiKey}&token=${token}`,
        {
          value: true,
        }
      )
      .then(() => {
        onDelete(list.id);
      })
      .catch((error) => {
        console.error("Error deleting list:", error);
      });
  };

  return (
    <Button variant="outlined" onClick={handleDelete}>
      Delete
    </Button>
  );
}

export default DeleteList;
