import React from "react";
import axios from "axios";
import Button from "@mui/material/Button";

function DeleteChecklist({ checklistId, onDelete }) {
  const handleDeleteChecklist = () => {
    const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
    const token =
      "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

    axios
      .delete(`https://api.trello.com/1/checklists/${checklistId}`, {
        params: {
          key: apiKey,
          token: token,
        },
      })
      .then(() => {
        onDelete(checklistId);
      })
      .catch((error) => {
        console.error("Error deleting checklist:", error);
      });
  };

  return (
    <Button variant="outlined" onClick={handleDeleteChecklist}>
      Delete Checklist
    </Button>
  );
}

export default DeleteChecklist;
