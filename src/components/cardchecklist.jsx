import React, { useState, useEffect } from "react";
import axios from "axios";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import CreateChecklist from "./createchecklist";
import { Paper, List, ListItem, ListItemText } from "@mui/material";
import DeleteChecklist from "./deletechecklist";
import ChecklistCheckItems from "./allcheckitems";
import CreateCheckItem from "./cretecheckitem";

function CardChecklistsModal({ cardId, onClose, cardName }) {
  const [checklists, setChecklists] = useState([]);
  const [createChecklistAnchorEl, setCreateChecklistAnchorEl] = useState(null);

  useEffect(() => {
    // Fetch checklists for the selected card
    const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
    const token =
      "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

    axios
      .get(
        `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${token}`
      )
      .then((response) => {
        setChecklists(response.data);
      })
      .catch((error) => {
        console.error("Error fetching checklists:", error);
      });
  }, [cardId]);

  const modalStyle = {
    position: "absolute",
    width: "400px",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: "white",
    padding: "20px",
  };

  const deleteStyle = {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  }

  const handleChecklistCreated = (newChecklist) => {
    setChecklists([...checklists, newChecklist]);
  };

  return (
    <Modal open={true} onClose={onClose}>
      <>
        <div style={modalStyle}>
          <h3>{cardName}</h3>

          <Paper elevation={3} style={{ padding: "16px" }}>
            <List>
              {checklists.map((checklist) => (
                <ListItem
                  key={checklist.id}
                  style={{ display: "flex", flexDirection: "column" }}
                >
                  <div
                    style={deleteStyle}
                  >
                    <ListItemText primary={checklist.name} />
                    <DeleteChecklist
                      checklistId={checklist.id}
                      onDelete={(deletedChecklistId) => {
                        // Update the state to remove the deleted checklist
                        const updatedChecklists = checklists.filter(
                          (checklist) => checklist.id !== deletedChecklistId
                        );
                        setChecklists(updatedChecklists);
                      }}
                    />
                  </div>
                  <ChecklistCheckItems cardId={cardId} checklistId={checklist.id}/>
                  <CreateCheckItem
                    checklistId={checklist.id}
                    onCheckItemAdded={(newCheckItem) => {
                      // Update the state with the added check item
                      const updatedChecklists = checklists.map((c) => {
                        if (c.id === checklist.id) {
                          return {
                            ...c,
                            checkItems: [...(c.checkItems || []), newCheckItem],
                          };
                        }
                        return c;
                      });
                      setChecklists(updatedChecklists);
                    }}
                    onClose={onClose}
                  />
                </ListItem>
              ))}
            </List>
          </Paper>

          <Button
            variant="outlined"
            onClick={(event) => setCreateChecklistAnchorEl(event.currentTarget)}
            style={{marginTop: 10, float: "right"}}
          >
            Create Checklist
          </Button>
          
        </div>
        {createChecklistAnchorEl && (
          <CreateChecklist
            cardId={cardId}
            onClose={() => setCreateChecklistAnchorEl(null)}
            anchorEl={createChecklistAnchorEl}
            handleClick={(event) =>
              setCreateChecklistAnchorEl(event.currentTarget)
            }
            onChecklistCreated={handleChecklistCreated}
          />
        )}
      </>
    </Modal>
  );
}

export default CardChecklistsModal;
