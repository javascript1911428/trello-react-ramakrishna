import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Card, CardContent, Typography, Grid } from "@mui/material";
import AddList from "./addinglist";
import DeleteList from "./deletelist";
import ListCards from "./listcards";
import AddCard from "./addcard";

function Lists() {
  const { boardId } = useParams();
  const [lists, setLists] = useState([]);
  const [boardName, setBoardName] = useState("");
  const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
  const token =
    "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${token}`
      )
      .then((response) => {
        setBoardName(response.data.name);
      })
      .catch((error) => {
        console.error("Error fetching board details:", error);
      });

    axios
      .get(
        `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${token}`
      )
      .then((response) => {
        setLists(response.data);
      })
      .catch((error) => {
        console.error("Error fetching lists:", error);
      });
  }, [boardId]);

  const handleListAdded = (newList) => {
    setLists([...lists, newList]);
  };

  const handleListDeleted = (listId) => {
    const updatedLists = lists.filter((list) => list.id !== listId);
    setLists(updatedLists);
  };

  const handleCardAdded = (newCard) => {
    console.log("New card added:", newCard);

    const name = newCard.name;
    const listId = newCard.listId;

    console.log(name);
    console.log(listId);

    const postData = {
      idList: listId,
      name: name,
      key: apiKey,
      token: token,
    };

    axios
      .post("https://api.trello.com/1/cards", null, {
        params: postData,
      })
      .then((response) => {
        console.log("Card added successfully:", response.data);
      })
      .catch((error) => {
        console.error("Error adding card:", error);
      });
  };

  const cardStyle = {
    maxWidth: 250,
    minHeight: 150,
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
  };

  return (
    <div>
      <h3>{boardName}</h3>
      <Grid container spacing={2}>
        {lists.map((list) => (
          <Grid item xs={12} sm={6} md={4} lg={2} key={list.id}>
            <Card sx={cardStyle}>
              <CardContent
                style={{ display: "flex", justifyContent: "space-between" }}
              >
                <Typography gutterBottom variant="h6" component="div">
                  {list.name}
                </Typography>

                <DeleteList
                  key={list.id}
                  list={list}
                  onDelete={handleListDeleted}
                />
              </CardContent>

              <ListCards listId={list.id} />
              <AddCard listId={list.id} onCardAdded={handleCardAdded} />
            </Card>
          </Grid>
        ))}
        <Grid item xs={12} sm={6} md={4} lg={3}>
          <AddList boardId={boardId} onListAdded={handleListAdded} />
        </Grid>
      </Grid>
    </div>
  );
}

export default Lists;
