import React, { useEffect, useState } from "react";
import axios from "axios";
import { Card, CardContent, Typography, Button } from "@mui/material";
import CardChecklistsModal from "./cardchecklist";

function ListCards({ listId }) {
  const [cards, setCards] = useState([]);
  const [selectedCard, setSelectedCard] = useState(null);
  const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
  const token =
    "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${token}`
      )
      .then((response) => {
        setCards(response.data);
      })
      .catch((error) => {
        console.error("Error fetching cards:", error);
      });
  }, [listId]);

  const handleDeleteCard = (cardId) => {
    axios
      .delete(
        `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${token}`
      )
      .then(() => {
        const updatedCards = cards.filter((card) => card.id !== cardId);
        setCards(updatedCards);
      })
      .catch((error) => {
        console.error("Error deleting card:", error);
      });
  };

  const handleCardClick = (cardId) => {
    setSelectedCard(cardId);
  };

  return (
    <div>
      {cards.map((card) => (
        <div key={card.id}>
          <Card
            style={{ margin: "8px 0", backgroundColor: "#FAFAFA" }}
            onClick={() => handleCardClick(card.id)} // Handle card click
          >
            <CardContent
              style={{ display: "flex", justifyContent: "space-between" }}
            >
              <Typography gutterBottom variant="h6" component="div">
                {card.name}
              </Typography>
            </CardContent>
          </Card>
          <Button variant="outlined" onClick={() => handleDeleteCard(card.id)}>
            Delete Card
          </Button>
          {/* Render the modal for the selected card */}
          {selectedCard === card.id && (
            <CardChecklistsModal
              cardId={selectedCard}
              cardName={card.name}
              onClose={() => setSelectedCard(null)} // Close the modal
            />
          )}
        </div>
      ))}
    </div>
  );
}

export default ListCards;
