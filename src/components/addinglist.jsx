import React, { useState } from "react";
import axios from "axios";
import {
  Card,
  CardContent,
  Typography,
  TextField,
  Button,
  Popover,
} from "@mui/material";

function AddList({ boardId, onListAdded }) {
  const [newListName, setNewListName] = useState("");
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);
  const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
  const token =
    "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

  const handleAddListClick = () => {
    setIsPopoverOpen(true);
  };

  const handleCreateNewList = () => {
    if (newListName) {
      axios
        .post(
          `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${token}`,
          {
            name: newListName,
          }
        )
        .then((response) => {
          onListAdded(response.data);
          setIsPopoverOpen(false);
          setNewListName("");
        })
        .catch((error) => {
          console.error("Error creating list:", error);
        });
    }
  };

  const handleClosePopover = () => {
    setIsPopoverOpen(false);
    setNewListName("");
  };

  const listCard = {
    maxWidth: 250,
    minHeight: 150,
  }

  return (
    <>
      <Card onClick={handleAddListClick} sx={listCard}>
        <CardContent>
          <Typography gutterBottom variant="h6" component="div">
            Add Another List
          </Typography>
        </CardContent>
      </Card>
      <Popover
        open={isPopoverOpen}
        onClose={handleClosePopover}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "center",
          horizontal: "center",
        }}
      >
        <div
          style={{ padding: "16px", display: "flex", flexDirection: "column" }}
        >
          {/* <Typography variant="h6">Create a New List</Typography> */}
          <TextField
            label="List Name"
            variant="outlined"
            value={newListName}
            onChange={(e) => setNewListName(e.target.value)}
            style={{ margin: "8px 0" }}
          />
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Button variant="outlined" onClick={handleClosePopover}>
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={handleCreateNewList}
              style={{ marginRight: "8px" }}
            >
              Add
            </Button>
          </div>
        </div>
      </Popover>
    </>
  );
}

export default AddList;
