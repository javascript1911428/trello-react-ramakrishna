import React, { useState } from "react";
import axios from "axios";
import { TextField, Button, Popover } from "@mui/material";

function CreateCheckItem({ checklistId, onCheckItemAdded, onClose }) {
  const [checkItemName, setCheckItemName] = useState("");
  const [addCheckItemAnchorEl, setAddCheckItemAnchorEl] = useState(null);

  const handleAddCheckItem = () => {
    if (checkItemName.trim() === "") {
      return;
    }
    const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
    const token =
      "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

    const apiUrl = `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${apiKey}&token=${token}&name=${checkItemName}`;

    axios
      .post(apiUrl)
      .then((response) => {
        onCheckItemAdded(response.data);
        setAddCheckItemAnchorEl(null);
        setCheckItemName("");
      })
      .catch((error) => {
        console.error("Error adding check item:", error);
      });
  };

  return (
    <>
      <Button
        variant="outlined"
        onClick={(event) => setAddCheckItemAnchorEl(event.currentTarget)}
      >
        +ADD CHECK ITEM
      </Button>

      <Popover
        open={Boolean(addCheckItemAnchorEl)}
        anchorEl={addCheckItemAnchorEl}
        onClose={() => setAddCheckItemAnchorEl(null)}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <div style={{ padding: "16px" }}>
          <TextField
            label="Check Item Name"
            variant="outlined"
            fullWidth
            value={checkItemName}
            onChange={(e) => setCheckItemName(e.target.value)}
          />
          <div style={{display: "flex", justifyContent: "space-between"}}>
            <Button
              variant="outlined"
              onClick={() => setAddCheckItemAnchorEl(null)}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={handleAddCheckItem}
            >
              Add
            </Button>
          </div>
        </div>
      </Popover>
    </>
  );
}

export default CreateCheckItem;
