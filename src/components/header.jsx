import trelloIcon from "../assets/trelloicon.svg"
import { Link, useNavigate } from "react-router-dom"
function Header() {
    const navigate = useNavigate()

    function handleLogoClick() {
        navigate("/")
    }

    return(
        <>
            <header>
                <img src={trelloIcon} alt="trello-icon" onClick={handleLogoClick}/>
            </header>
        </>
    )
}

export default Header