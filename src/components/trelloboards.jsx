import React, { useState, useEffect } from "react";
import { getBoards, createBoard } from "../api.jsx";

import {
  Card,
  CardContent,
  Typography,
  Grid,
  Button,
  Popover,
  TextField,
} from "@mui/material";
import { Link } from "react-router-dom";

const TrelloBoards = () => {
  const [boards, setBoards] = useState([]);
  const [creatingBoard, setCreatingBoard] = useState(false);
  const [newBoardName, setNewBoardName] = useState("");
  const [anchorEl, setAnchorEl] = useState(null);

  useEffect(() => {
    getBoards()
      .then((response) => {
        setBoards(response.data);
      })
      .catch((error) => {
        console.error("Error fetching Trello boards:", error);
      });
  }, []);

  const addNewBoard = (newBoard) => {
    setBoards([...boards, newBoard]);
  };

  const cardStyle = {
    maxWidth: 250,
    minHeight: 150,
    backgroundImage:
      'url("https://images.unsplash.com/photo-1472289065668-ce650ac443d2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2069&q=80")',
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    textDecoration: "none",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  };

  const cardStyle1 = {
    maxWidth: 250,
    minHeight: 150,
    backgroundColor: "#F1F2F4",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  };

  const contentStyle = {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    height: "100%",
    textDecoration: "none",
  };

  const handleCreateBoardClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCreateNewBoard = () => {
    if (newBoardName) {
      createBoard(newBoardName)
        .then((response) => {
          addNewBoard(response.data);
          setCreatingBoard(false);
          setNewBoardName("");
          setAnchorEl(null);
        })
        .catch((error) => {
          console.error("Error creating board:", error);
        });
    }
  };

  const handleClosePopover = () => {
    setAnchorEl(null);
    setCreatingBoard(false);
    setNewBoardName("");
  };

  return (
    <>
      <div>
        <h2>Trello Boards</h2>
        <Grid container spacing={2} padding={6}>
          {boards.map((board) => (
            <Grid item xs={12} sm={6} md={4} lg={2} key={board.id}>
              <Link
                to={`/boards/${board.id}`}
                style={{ textDecoration: "none" }}
                key={board.id}
              >
                <Card sx={cardStyle}>
                  <CardContent sx={contentStyle}>
                    <Typography gutterBottom variant="h5" component="div">
                      {board.name}
                    </Typography>
                  </CardContent>
                </Card>
              </Link>
            </Grid>
          ))}
          <Grid item xs={12} sm={6} md={4} lg={2}>
            <Card
              sx={cardStyle1}
              onClick={(e) => handleCreateBoardClick(e)}
              style={{ cursor: "pointer" }}
            >
              <CardContent sx={contentStyle}>
                <Typography gutterBottom variant="h5" component="div">
                  Create a Board
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>

      

      <Popover
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClosePopover}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "bottom",
        }}
      >
        <div
          style={{
            padding: "16px",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          }}
        >
          <TextField
            label="Create new board"
            variant="outlined"
            value={newBoardName}
            onChange={(e) => setNewBoardName(e.target.value)}
            style={{ margin: "8px 0" }}
          />
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Button variant="outlined" onClick={handleClosePopover}>
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={handleCreateNewBoard}
              style={{ marginRight: "8px" }}
            >
              Create
            </Button>
          </div>
        </div>
      </Popover>
    </>
  );
};

export default TrelloBoards;
