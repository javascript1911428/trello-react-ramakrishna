import React, { useState, useEffect } from "react";
import axios from "axios";
import { List, ListItem, Button, LinearProgress } from "@mui/material";
import DeleteCheckItem from "./deletecheckItem";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

function ChecklistCheckItems({ cardId, checklistId }) {
  const [checkItems, setCheckItems] = useState([]);
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
    const token =
      "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

    axios
      .get(
        `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${apiKey}&token=${token}`
      )
      .then((response) => {
        console.log(response.data);
        setCheckItems(response.data);
        updateProgress(response.data);
      })
      .catch((error) => {
        console.error("Error fetching check items:", error);
      });
  }, [checklistId]);

  const updateProgress = (items) => {
    if (items.length === 0) {
      setProgress(0);
    } else {
      const completedItems = items.filter((item) => item.state === "complete");
      const completionPercentage = (completedItems.length / items.length) * 100;
      setProgress(completionPercentage);
    }
  };

  const handleCheckboxChange = (checkItemId) => {
    const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
    const token =
      "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";

    const updatedCheckItems = checkItems.map((checkItem) => {
      if (checkItem.id === checkItemId) {
        // Toggle the state
        checkItem.state =
          checkItem.state === "complete" ? "incomplete" : "complete";

        // Update the check item's state in Trello
        axios
          .put(
            `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${checkItem.state}&key=${apiKey}&token=${token}`
          )
          .then((response) => {
            console.log("Check item state updated:", response.data);
          })
          .catch((error) => {
            console.error("Error updating check item state:", error);
          });
      }
      return checkItem;
    });

    setCheckItems(updatedCheckItems);
    updateProgress(updatedCheckItems);
  };

  const handleDeleteCheckItem = (deletedCheckItemId) => {
    const updatedCheckItems = checkItems.filter(
      (checkItem) => checkItem.id !== deletedCheckItemId
    );
    setCheckItems(updatedCheckItems);
    updateProgress(updatedCheckItems);
  };

  return (
    <div>
      <LinearProgress variant="determinate" value={progress} style={{marginTop: 8, width: "100%"}}/>
      <List >
        {checkItems.map((checkItem) => (
          <ListItem
            key={checkItem.id}
            onClick={() => handleCheckboxChange(checkItem.id)}
          >
            {checkItem.state === "complete" ? (
              <CheckBoxIcon />
            ) : (
              <CheckBoxOutlineBlankIcon />
            )}
            {checkItem.name}
            <DeleteCheckItem
              checklistId={checklistId}
              checkItemId={checkItem.id}
              onDelete={handleDeleteCheckItem}
              style={{marginLeft: 10}}
            />
          </ListItem>
        ))}
      </List>
    </div>
  );
}

export default ChecklistCheckItems;
