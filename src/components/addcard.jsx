import React, { useState } from "react";
import { Button, Popover, TextField, Box, Typography } from "@mui/material";

function AddCard({ listId, onCardAdded }) {
  const [anchorEl, setAnchorEl] = useState(null);
  const [newCardName, setNewCardName] = useState("");

  const handleOpenPopover = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClosePopover = () => {
    setAnchorEl(null);
    setNewCardName("");
  };

  const handleCardNameChange = (event) => {
    setNewCardName(event.target.value);
  };

  const handleAddCard = () => {
    if (newCardName.trim() === "") {
      return;
    }
    onCardAdded({ name: newCardName, listId });

    handleClosePopover();
  };


  return (
    <div>
      <Button variant="outlined" onClick={handleOpenPopover}>
        + Add Card
      </Button>
      <Popover
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClosePopover}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <Box p={2}>
          <Typography variant="h6" gutterBottom>
            Add a New Card
          </Typography>
          <TextField
            label="Card Name"
            variant="outlined"
            fullWidth
            value={newCardName}
            onChange={handleCardNameChange}
          />
          <Box mt={2}>
            <Button variant="contained" color="primary" onClick={handleAddCard}>
              Add
            </Button>
            <Button variant="outlined" onClick={handleClosePopover}>
              Cancel
            </Button>
          </Box>
        </Box>
      </Popover>
    </div>
  );
}

export default AddCard;
