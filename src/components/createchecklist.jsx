import React, { useState } from "react";
import Popover from "@mui/material/Popover";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import axios from "axios";

const popoverStyle = {
  width: "400px",
  backgroundColor: "white",
  padding: "20px",
};

function CreateChecklist({ cardId, onClose, anchorEl, handleClick, onChecklistCreated}) {
  const [checklistName, setChecklistName] = useState("");

  const handleClose = () => {
    onClose();
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  const handleCreateChecklist = () => {
    // Create a checklist using Axios
    const apiKey = "cc546cc39e0708ffd1affcb5b35a1509";
    const token =
      "ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE";
    axios
      .post(
        `https://api.trello.com/1/checklists?idCard=${cardId}&key=${apiKey}&token=${token}`,
        {
          name: checklistName,
        }
      )
      .then((response) => {
        onChecklistCreated(response.data)
        handleClose();
      })
      .catch((error) => {
        console.error("Error creating checklist:", error);
      });
  };

  return (
    <Popover
      id={id}
      open={open}
      anchorEl={anchorEl}
      onClose={handleClose}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "center",
      }}
    >
      <>
        <div style={popoverStyle}>
          <h2>Create Checklist</h2>
          <TextField
            label="Checklist Name"
            variant="outlined"
            fullWidth
            value={checklistName}
            onChange={(e) => setChecklistName(e.target.value)}
          />
          <div style={{ marginTop: "20px" }}>
            <Button variant="outlined" onClick={handleClose}>
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              style={{ marginLeft: "10px" }}
              onClick={handleCreateChecklist}
            >
              Create
            </Button>
          </div>
        </div>
      </>
    </Popover>
  );
}

export default CreateChecklist;
