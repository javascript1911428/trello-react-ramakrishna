import axios from 'axios';

const apiKey = 'cc546cc39e0708ffd1affcb5b35a1509';
const token = 'ATTA34a5465e40a2fe99344dcd5f930e008f242d75c93f1eaaf839008bcf5963b18d5F2C40CE';

export const getBoards = () => {
  return axios.get(`https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`);
};

export const createBoard = (boardName) => {
  const requestBody = { name: boardName };
  return axios.post(`https://api.trello.com/1/boards?key=${apiKey}&token=${token}`, requestBody);
};
